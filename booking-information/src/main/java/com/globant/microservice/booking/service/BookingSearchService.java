package com.globant.microservice.booking.service;

import com.globant.microservice.booking.model.domain.Booking;

/**
 * Created by s.scatularo on 10/23/15.
 */
public interface BookingSearchService {
    Booking getBooking(String query);
}
