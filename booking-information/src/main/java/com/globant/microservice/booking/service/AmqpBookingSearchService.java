package com.globant.microservice.booking.service;

import com.globant.microservice.booking.configuration.BookingConfiguration;
import com.globant.microservice.booking.model.domain.Booking;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by s.scatularo on 10/23/15.
 */
@Service
public class AmqpBookingSearchService implements BookingSearchService {
    private static final String BOOKING_SEARCH_EXCHANGE = "exchange-booking-search";
    private static final String BOOKING_SEARCH_QUEUE = "queue-booking-search";

    @Autowired
    private AmqpTemplate template;

    @Override
    public Booking getBooking(String query) {
        Object s = template.convertSendAndReceive(BOOKING_SEARCH_EXCHANGE, "*", query);
        /*template.convertAndSend(BOOKING_SEARCH_EXCHANGE, "*", query);
        Object s = template.receiveAndConvert(BOOKING_SEARCH_QUEUE);*/
        LoggerFactory.getLogger(getClass()).info("{}", s);
        return new Booking();
    }
}
