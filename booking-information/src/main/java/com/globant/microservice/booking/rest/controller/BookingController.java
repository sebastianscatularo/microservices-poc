package com.globant.microservice.booking.rest.controller;

import com.globant.microservice.booking.model.domain.Booking;
import com.globant.microservice.booking.service.BookingSearchService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by s.scatularo on 10/23/15.
 */
@RestController
@RequestMapping("/bookings")
public class BookingController {
    @Autowired
    private BookingSearchService searchService;

    @Autowired
    private AmqpTemplate template;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Booking> booking(@RequestParam("query") String query) {
        template.convertAndSend("Initializing query");
        //Booking booking = searchService.getBooking(query);
        return ResponseEntity.ok(new Booking());
    }
}
