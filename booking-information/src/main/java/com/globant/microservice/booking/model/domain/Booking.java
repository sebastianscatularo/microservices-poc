package com.globant.microservice.booking.model.domain;

/**
 * Created by s.scatularo on 10/23/15.
 */
public class Booking {
    private String nil;

    public Booking() {

    }

    public String getNil() {
        return nil;
    }

    public void setNil(String nil) {
        this.nil = nil;
    }
}
