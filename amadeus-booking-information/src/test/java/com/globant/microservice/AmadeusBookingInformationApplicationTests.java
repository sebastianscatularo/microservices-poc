package com.globant.microservice;

import com.globant.microservice.booking.amadeus.AmadeusBookingApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AmadeusBookingApplication.class)
public class AmadeusBookingInformationApplicationTests {

	@Test
	public void contextLoads() {
	}

}
