package com.globant.microservice.booking.amadeus.ws;

import com.globant.microservice.booking.amadeus.model.entity.Booking;

/**
 * Created by s.scatularo on 10/23/15.
 */
public class SearchBookingResponse {
    private final Booking booking;

    public SearchBookingResponse(Booking booking) {
        this.booking = booking;
    }
}
