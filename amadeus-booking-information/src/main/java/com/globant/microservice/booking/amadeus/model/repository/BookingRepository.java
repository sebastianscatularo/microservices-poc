package com.globant.microservice.booking.amadeus.model.repository;

import com.globant.microservice.booking.amadeus.model.entity.Booking;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by s.scatularo on 10/23/15.
 */
public interface BookingRepository extends CrudRepository<Booking, Long> {
    Booking findByRecordLocator(String recordLocator);
}
