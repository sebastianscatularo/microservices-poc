package com.globant.microservice.booking.amadeus.model;

import com.globant.microservice.booking.amadeus.model.entity.Booking;
import com.globant.microservice.booking.amadeus.model.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by s.scatularo on 10/23/15.
 */
@Component
public class BookingFixture {
    @Autowired
    private BookingRepository bookingRepository;

    @PostConstruct
    public void init() {
        bookingRepository.save(new Booking());
    }
}
