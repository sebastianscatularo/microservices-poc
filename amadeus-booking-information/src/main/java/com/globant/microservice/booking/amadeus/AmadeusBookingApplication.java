package com.globant.microservice.booking.amadeus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmadeusBookingApplication {
    public static void main(String[] args) {
        SpringApplication.run(AmadeusBookingApplication.class, args);
    }
}
