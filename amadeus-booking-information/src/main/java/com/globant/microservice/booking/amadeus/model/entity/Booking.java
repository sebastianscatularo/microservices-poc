package com.globant.microservice.booking.amadeus.model.entity;

import javax.persistence.*;

/**
 * Created by s.scatularo on 10/23/15.
 */
@Entity
@Table
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "RECORD_LOCATOR")
    private String recordLocator;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
