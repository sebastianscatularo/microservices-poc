package com.globant.microservice.booking.amadeus.amqp;

import com.globant.microservice.booking.amadeus.ws.SearchBookingResponse;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * Created by s.scatularo on 10/23/15.
 */
@Service
public class SearchBookingInformation {
//    @RabbitListener()
    public SearchBookingResponse searchBooking() {
        return new SearchBookingResponse(null);
    }
}
