package com.globant.microservice.booking.amadeus.ws;

/**
 * Created by s.scatularo on 10/23/15.
 */
public class SearchCountryRequest {
    private String recordLocator;

    public String getRecordLocator() {
        return recordLocator;
    }

    public void setRecordLocator(String recordLocator) {
        this.recordLocator = recordLocator;
    }
}
