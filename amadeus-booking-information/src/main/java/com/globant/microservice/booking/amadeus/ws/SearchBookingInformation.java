package com.globant.microservice.booking.amadeus.ws;

import com.globant.microservice.booking.amadeus.model.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Created by s.scatularo on 10/23/15.
 */
@Endpoint
public class SearchBookingInformation {
    @Autowired
    private BookingRepository repository;

    @ResponsePayload
    public SearchBookingResponse getBooking(@RequestPayload SearchCountryRequest request) {
        SearchBookingResponse response = new SearchBookingResponse(repository.findByRecordLocator(request.getRecordLocator()));
        return response;
    }
}
