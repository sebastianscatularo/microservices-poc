package com.globant.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeyPerformanceIndicatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(KeyPerformanceIndicatorApplication.class, args);
    }
}
