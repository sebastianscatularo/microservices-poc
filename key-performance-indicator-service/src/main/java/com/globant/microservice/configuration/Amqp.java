package com.globant.microservice.configuration;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by sebastian on 25/10/15.
 */
@Configuration
public class Amqp {
    @Value("${com.cloud.amqp.port}")
    private int port;
    @Value("${com.cloud.amqp.username}")
    private String username;
    @Value("${com.cloud.amqp.password}")
    private String password;
    @Value("${com.cloud.amqp.vhost}")
    private String virtualHost;
    @Value("${com.cloud.amqp.host}")
    private String host;
    @Value("exchange.name")
    private String exchangeName;
    @Value("${queue.name}")
    private String queueName;

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
        connectionFactory.setCacheMode(CachingConnectionFactory.CacheMode.CHANNEL);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(username);
        connectionFactory.setRequestedHeartBeat(30);
        connectionFactory.setConnectionTimeout(30000);
        return connectionFactory;
    }

    @Bean
    public RabbitAdmin admin() {
        RabbitAdmin admin = new RabbitAdmin(connectionFactory());
        FanoutExchange exchange = new FanoutExchange(exchangeName);
        admin.declareExchange(exchange);
        //for (String queue : queueName) {
            Queue queue = new Queue(queueName);
            admin.declareQueue(queue);
            admin.declareBinding(BindingBuilder.bind(queue).to(exchange));
        //}
        return admin;
    }
}
