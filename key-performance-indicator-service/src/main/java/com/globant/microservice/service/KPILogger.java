package com.globant.microservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Created by sebastian on 25/10/15.
 */
@Component
public class KPILogger {
    private static final Logger log = LoggerFactory.getLogger(KPILogger.class);

    @RabbitListener(queues = "${queue.name}")
    public void doLog(String message) {
        log.info(message);
    }
}
