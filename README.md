## Start up

> mvn spring-boot:run -f key-performance-indicator-service/pom.xml 

> mvn spring-boot:run -f booking-information/pom.xml 

> curl http://localhost:8080/bookings

* Example case:

> An event produced on booking-information should be logged on key-performance-indicator-service through an AMQP exchange/queue

## // TODO

* RPC call to retrieve booking information over AMQP
* SOAP endpoint to retrieve booking information over HTTP
* Benchmark for both approaches
 